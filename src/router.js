import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Signup from './views/Signup.vue'
import Profile from './views/Profile.vue'
import pageShowtime from './views/pageShowtime.vue'
import pageSeat from './views/pageSeat.vue'
import Reserve from './views/Reserve.vue'
import Booking from './views/Booking.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/Profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/Showtime',
      name: 'Showtime',
      component: pageShowtime
    },
    {
      path: '/Seat',
      name: 'Seat',
      component: pageSeat
    },
    {
      path: '/Reserve',
      name: 'Reserve',
      component: Reserve
    },
    {
      path: '/Booking',
      name: 'Booking',
      component: Booking
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
